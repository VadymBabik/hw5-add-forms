import { CHECKOUT } from "./actions";

const initialState = {
  checkout: [],
};

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECKOUT:
      const checkout =[...state.checkout, action.payload]
      console.log(checkout)
      return { ...state, checkout: checkout };

    default:
      return state;
  }
};
