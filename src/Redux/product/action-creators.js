import {
  LOADING_CARDS,
  REMOVE_PRODUCT_CART,
  LOADING_ERROR,
  ADD_TO_CART,
  REMOVE_ONE_CART,
  FETCH_CARTS_SUCCESS,
  TOGGLE_FAVORITE,
  CLEAR_CART,
} from "./actions";

export const isLoading = () => ({ type: LOADING_CARDS });
export const isError = (payload) => ({ type: LOADING_ERROR, payload });
export const fetchCarts = (payload) => ({ type: FETCH_CARTS_SUCCESS, payload });
export const favorite = (payload) => ({ type: TOGGLE_FAVORITE, payload });
export const addInCart = (payload) => ({ type: ADD_TO_CART, payload });
export const removeOneCart = (payload) => ({ type: REMOVE_ONE_CART, payload });
export const removeCart = (payload) => ({ type: REMOVE_PRODUCT_CART, payload });
export const clearCart = (payload) => ({ type: CLEAR_CART, payload });
